def reactionRate(S, VMax, KM):
    """
    Returns rates for Michaelis-Menten model
    """
    return VMax*S/(KM+S)

def get_likelihood(VMax, KM, rate_data):
    """
    Returns likelihood and log-likelihood for Michaelis-Menten model and Gaussian errors.
    """
    S=rate_data[:,0]
    nu=rate_data[:,1]
    sigma=rate_data[:,2]
    logL=np.sum(-0.5*((reactionRate(S, VMax, KM)-nu)/sigma)**2)
    L=np.exp(logL)
    return  L, logL

#helper function for fitting using global value for KM
def reactionRate_VMax(S, VMax):
    return reactionRate(S, VMax, KM)