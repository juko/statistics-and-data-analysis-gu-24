import numpy as np
import scipy.stats 

def sample_from_Gauss(*, n_steps, mu, sigma, x_old, max_step_size, sample_every=1):
    p_old=scipy.stats.norm.pdf(x_old, mu, sigma)
    x_MC=[]
    for i in range(n_steps):
        x_new = x_old+np.random.uniform(-max_step_size, max_step_size)
        p_new=scipy.stats.norm.pdf(x_new, mu, sigma)
        if np.random.uniform()<p_new/p_old:
            x_old=x_new
            p_old=p_new
        if i% sample_every==0:
            x_MC.append(x_old)
    x_MC=np.asarray(x_MC)
    return x_MC
