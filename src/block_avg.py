"""
Author: Juergen Koefinger, Frankfurt am Main, Germany (2018)
"""
import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from numba import jit

def get_bootstrap_error(x, M_list=[],  Nb=100):
    """
    x: Observable 
    M_list: List containting numbers of blocks.
    Nb: Numbe of repeats for resampling 
    Returns: Estimates for standard error of the mean as function of block lengths
    """
    N=len(x)
    n_list=[]
    if len(M_list)==0:
        M_list=np.asarray(range(2,40))
    bte=[]
   
    for M in M_list:
        n=int((float(N)/float(M)))
        print(n*M)
        n_list.append(n)
        tmp=np.split(x[:n*M], M)
        block_means=np.mean(tmp, axis=1)
        tmp=[]
        for i in range(Nb):
            bm=np.random.choice(block_means, M)
            mean=bm.mean()
            tmp.append(mean)
        tmp=np.asarray(tmp)
        mean=tmp.mean()
        var=((tmp-mean)**2).sum()/float(Nb-1)
        std=np.sqrt(var)
        bte.append([n, std])
    return np.asarray(bte)

def get_blocked_std(x, M_list=[]):
    """
    x: Observable 
    M_list: List containting numbers of blocks
    Returns: Estimates for standard error of the mean as function of block lengths
    """
    N=len(x)
    n_list=[]
    if len(M_list)==0:
        M_list=np.asarray(range(2,100))
    bse=[]
    for M in M_list:
        n=int((float(N)/float(M)))
        n_list.append(n)
        tmp=np.split(x[:n*M], M)
        block_means=np.mean(tmp, axis=1)
        mean=block_means.mean()
        var=((block_means-mean)**2).sum()/float(M-1.)
        std=np.sqrt(var)
        bse.append([n, std/np.sqrt(M)])
    return np.asarray(bse)

def plateau(x, a):
    return a 

def fit_plateau(stderr, init_value, fit_range=(), plot=True, label=""):
    """
    Fitting plateau of blocked standard error or standard error from resampling for varying block length. 
    
    stderr: 2d-array (block length, std. err.)
    init_value: initial value of plateau height for fitting
    fit_range:  range of block length where std. error is approximately constant 
                (at larger block length for blocked error, at smaller block length for resampling)
    """
    ind=np.where(np.logical_and(stderr[:,0]>fit_range[0], stderr[:,0]<fit_range[1]))[0]
    par, cov = curve_fit(plateau, stderr[ind,0], stderr[ind,1], p0=[init_value])    
    plateau_value=par[0]
    if plot:

        line, = plt.plot(stderr[:,0], stderr[:,1], '.-', label=label)
        plt.axhline(plateau_value, ls='--',color=line.get_color(), label="plateau")
        plt.xscale('log')
        plt.xlabel("block length")
        plt.ylabel("standard error")
        plt.legend(ncol=2, fontsize=16)
    return plateau_value

@jit
def get_acf(a, N, c, normed=False):
    n=len(c)
    for i in range(n):
        for j in range(0, N-i):
            c[i]+=a[j]*a[j+i]
        c[i]/=float(N-i)
    if normed==True:
        c/=c[0]
    return c
